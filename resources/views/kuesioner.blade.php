<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('elearning/css/dvn.css') }}" />

</head>
<body>
    <div class="container mt-5 ">
        <div class="card form_border">
            <div class="card-head dd text-white form_border">
                <h1>Kuesioner Dosen</h1>
             </div>
<form action="" method="post" class = "for">
                <div class="card-body">
                    <div class = "form-group">
                        <label for=""> <b>Pertanyaan</b></label>    
                        <input type="text" class ="form-control" name="pertanyaan" placeholder="Pertanyaan">
                    </div>
                </div>
                
                <div class= "card-body">
                    <div class ="form-group">
                    <label for=""><b>Aktif</b> </label>    
                    <input type="text" class ="form-control" name="aktif" placeholder="Pertanyaan">
                    </div>
                </div>
    
                <div class= "card-body">
                    <div class ="form-group">
                    <label for=""><b>Nilai</b> </label>  <br>  
                    
                    <input type="radio" name="nilai" value ="1" id="" >
                    <label for="" class="mr-5">1</label>

                    <input type="radio" name="nilai" value ="2" id="">
                    <label for=""class="mr-5">2</label>

                    <input type="radio" name="nilai" value ="3" id="">
                    <label for=""class="mr-5">3</label>

                    <input type="radio" name="nilai" value ="4" id="">
                    <label for=""class="mr-5">4</label>

                    <input type="radio" name="nilai" value ="5" id="">
                    <label for=""class="mr-5">5</label>
                    </div>
                </div>
                <div class="d-flex justify-content-center mb-4">
                <button type="submit" class = "btn btn-dd text-white ">Input</button>
                </div>
                
            </div>
</form>
        </div>
    </div>
    
    
    
    
    
    
    
    
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>