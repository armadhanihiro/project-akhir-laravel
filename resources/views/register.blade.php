<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="{{ asset('elearning/css/style.css') }}" />
    <title>Form Register</title>
  </head>
  <body>
    <div class="container">
      <h1>Register</h1>
      <form>
        <div class="form-control">
          <input type="text" required>
          <label>Nama Lengkap</label>
        </div>

        <div class="form-control">
          <input type="text" required>
          <label>Email</label>
        </div>

        <div class="form-control">
          <input type="password" required>
          <label>Password</label>
        </div>

        <button class="btn pb-4">Sign Up</button>

      </form>
    </div>
    <script src="{{ asset('elearning/js/script.js') }}"></script>
  </body>
</html>
