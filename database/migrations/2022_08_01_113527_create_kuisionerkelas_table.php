<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKuisionerkelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kuisionerkelas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nim',10);
            
            $table->unsignedBigInteger('kelas_id');
            $table->unsignedBigInteger('pertanyaan_id');
            $table->unsignedBigInteger('jawaban_id');
            $table->foreign('kelas_id')
                    ->references('id')->on('kelas')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->foreign('pertanyaan_id')
                    ->references('id')->on('kuisionerdosen')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->foreign('jawaban_id')
                    ->references('id')->on('jawabankuisioner')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->string('jawaban_text',45);         
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kuisionerkelas');
    }
}
