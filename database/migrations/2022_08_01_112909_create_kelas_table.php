<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode',20);
            $table->string('semester',5);
            $table->unsignedBigInteger('dosen_id');
            $table->unsignedBigInteger('matakuliah_id');
            $table->unsignedBigInteger('prodi_id');
            $table->foreign('dosen_id')
                    ->references('id')->on('dosen')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->foreign('matakuliah_id')
                    ->references('id')->on('matakuliah')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->foreign('prodi_id')
                    ->references('id')->on('prodi')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kelas');
    }
}
